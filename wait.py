#!/usr/bin/env python3
import sys
from datetime import datetime
from telnetlib import Telnet


def wait_net_service(server, port, timeout=1):
    """
    Wait for network service to appear
    @return: True of False, if timeout is None may return only True or throw unhandled network exception
    :type timeout: in seconds, if None or 0 wait forever
    :param port: port
    :param server: server address
    """
    first = datetime.now()
    attempts = 0
    while True:
        diff = datetime.now() - first
        attempts = attempts + 1
        if diff.total_seconds() >= timeout:
            return False
        try:
            Telnet(server, port, 1)
            print("Connected after {} seconds".format(int(diff.total_seconds())))
            return True
        except Exception as e:
            print(e)


if __name__ == '__main__':
    wait_net_service(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]))
