from django.db.models import Sum
from api import models

""""
def km_abastecido_mes(inicio:datetime, fim:datetime):
    return models.Abastecimento.objects.filter(
        data_abastecimento__range=(inicio, fim)
    ).count()


"""


def count_supply_month(month, year):
    return models.Supply.objects.filter(
        date_supply__month=month,
        date_supply__year=year
    ).count()


def calculate_km_supply_month(month, year):
    query_set = models.Supply.objects.filter(
        date_supply__month=month,
        date_supply__year=year
    ).order_by('date_supply')

    first_supply = query_set.first()
    last_supply = query_set.last()

    diff_km_supply = last_supply.km_supply - first_supply.km_supply
    return diff_km_supply


def sum_quantity_liters_supply_month(month, year):
    return models.Supply.objects.filter(
        date_supply__month=month,
        date_supply__year=year
    ).aggregate(quantity_liters=Sum('quantity_liters', field="quantity_liters"))


def calculate_km_liters(month, year):
    return calculate_km_supply_month(month, year) / sum_quantity_liters_supply_month(month, year)['quantity_liters']
